from typing import Dict, Text

import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer


class UnsupportedVectorizer(Exception):

    def __init__(self, vectorizer_name):

        self.msg = f'Unsupported vectorizer {vectorizer_name}'
        super().__init__(self.msg)


def get_supported_vectorizer() -> Dict:
    """Get supported vectorizers.

    Returns
    -------
    Dict
        Supported vectorizers.

    """
    return {
        'tfidf': TfidfVectorizer,
    }


def vectorize(dataset: pd.DataFrame,
              vectorizer_name: Text,
              vectorizer_params: Dict,
              ):
    """Vectorize data.

    Parameters
    ----------
        dataset : pd.DataFrame
            Dataset.
        vectorizer_name : Text
            Estimator name.
        vectorizer_params : Dict
            Vectorizer params.

    Returns
    ----------
        vectorized dataset.

    """
    vectorizers = get_supported_vectorizer()

    if vectorizer_name not in vectorizers.keys():
        raise UnsupportedVectorizer(vectorizer_name)
    vectorizer = vectorizers[vectorizer_name](**vectorizer_params)
    vectorized_dataset = vectorizer.fit_transform(dataset['text'])
    vectorized_dataset = pd.DataFrame(vectorized_dataset.toarray())
    vectorized_dataset['target'] = dataset['target']

    return vectorized_dataset
