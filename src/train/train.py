from typing import Dict, Text

import pandas as pd
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import f1_score, make_scorer
from sklearn.linear_model import LogisticRegression


class UnsupportedClassifier(Exception):

    def __init__(self, estimator_name):

        self.msg = f'Unsupported estimator {estimator_name}'
        super().__init__(self.msg)


def get_supported_estimator() -> Dict:
    """Get supported estimator.

    Returns
    -------
    Dict
        Supported classifiers.

    """
    return {
        'logreg': LogisticRegression,
    }


def train(df: pd.DataFrame,
          target_column: Text,
          estimator_name: Text,
          param_grid: Dict,
          cv: int):
    """Train model.

    Parameters
    ----------
        df : pd.DataFrame
            Dataset.
        target_column : Text
            Target column name.
        estimator_name : Text
            Estimator name.
        param_grid : Dict
            Grid parameters
        cv : int
            Cross-validation value.

    Returns
    ----------
        Trained model.

    """
    estimators = get_supported_estimator()

    if estimator_name not in estimators.keys():
        raise UnsupportedClassifier(estimator_name)

    estimator = estimators[estimator_name]()
    f1_scorer = make_scorer(f1_score, average='weighted')
    clf = GridSearchCV(estimator=estimator,
                       param_grid=param_grid,
                       cv=cv,
                       verbose=1,
                       scoring=f1_scorer)

    y_train = df.loc[:, target_column].values.astype('int32')
    x_train = df.drop(target_column, axis=1).values.astype('float32')
    clf.fit(x_train, y_train)

    return clf
