import argparse
import pandas as pd
from typing import Text
import yaml
import re
import string

from src.featurize.vectorize import vectorize
from src.utils.logs import get_logger


def text_preprocessing(text):
    """Cleaning the text.

    """
    text = text.lower()
    text = re.sub('\[.*?\]', '', text)
    text = re.sub('https?://\S+|www\.\S+', '', text)
    text = re.sub('<.*?>+', '', text)
    text = re.sub('[%s]' % re.escape(string.punctuation), '', text)
    text = re.sub('\n', '', text)
    text = re.sub('\w*\d\w*', '', text)
    return text


def featurize_data(config_path: Text) -> None:
    """Create new features.
    Args:
        config_path {Text}: path to config
    """

    with open(config_path) as conf_file:
        config = yaml.safe_load(conf_file)

    logger = get_logger('FEATURIZE', log_level=config['base']['log_level'])

    logger.info('Load raw data')
    dataset = pd.read_csv(config['data_load']['dataset_csv'])

    logger.info('Extract features')
    dataset['text'] = dataset['text'].apply(lambda x: text_preprocessing(x))
    featured_dataset = dataset[[
        'text',
        'target',
    ]]

    vectorizer_name = config['featurize']['vectorizer_name'],
    vectorizer_name = vectorizer_name[0]
    featured_dataset = vectorize(
        dataset=featured_dataset,
        vectorizer_name=vectorizer_name,
        vectorizer_params=config[
            'featurize']['vectorizers'][vectorizer_name]['vectorizer_params']
    )
   
    logger.info('Save features')
    features_path = config['featurize']['features_path']
    featured_dataset.to_csv(features_path, index=False)


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    featurize_data(config_path=args.config)
