# How to Run 

## 1. Fork / Clone repo
- fork to your personal repo;
- clone to you local machine.

## 2. Data source
- download disaster tweets dataser from kaggle. 

## 3. Run with Docker 
Create config/.env
```bash
GIT_CONFIG_USER_NAME=<git user>
GIT_CONFIG_EMAIL=<git email>
```
example:

```config/.env
GIT_CONFIG_USER_NAME=ivan.varlamov
GIT_CONFIG_EMAIL=xxxyxxx16@gmail.com
```

Build
```bash
docker-compose  --env-file config/.env build
```

Run 
```bash
docker-compose run --name container_name --rm -p 8888:8888 disaster_tweets
```

Open notebook:
- open http://localhost:8888 in browser.

Run in notebook cell ml pipeline:
```build
dvc repro
```
